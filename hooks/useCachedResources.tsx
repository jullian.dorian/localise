import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { useEffect, useState } from 'react';
import {UserProps} from "../auth/auth";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        await SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'opensans': require('../assets/fonts/OpenSans-Regular.ttf')
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        await SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}

export function useUser(): [loadingUserComplete: boolean, user: UserProps|undefined, setUser: (v: UserProps) => Promise<UserProps>] {
  const [user, setUser] = useState<UserProps>({
    id: -1,
    picture: null,
    email: ""
  });
  const [loadingUserComplete, isLoading] = useState<boolean>(false);

  const updateUser = async (v: UserProps): Promise<UserProps> => {
    setUser(v);
    await AsyncStorage.setItem("user", JSON.stringify(v));
    return v;
  }

  useEffect(() => {
    (async () => {
      try {
        const u: string | null = await AsyncStorage.getItem("user");
        if (u) {
          setUser(JSON.parse(u));
        }
      } catch (_) {

      } finally {
        isLoading(true);
      }

    })();
  }, [])

  return [loadingUserComplete, user, updateUser];
}
