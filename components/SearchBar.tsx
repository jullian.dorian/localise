import {FontAwesomeIcon, FontAwesomeIconStyle, Props} from '@fortawesome/react-native-fontawesome';
import React from 'react';
import {StyleProp, StyleSheet, TextInput, TextStyle, View, ViewStyle} from "react-native";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {Colors} from "../constants/Utils";

function SearchBar(props: SearchBarProps) {
    return (
        <View style={[SearchBarStyle.searchBar, props.style]}>
            <FontAwesomeIcon icon={faSearch} style={[SearchBarStyle.icon, props.iconStyle]} {...props.iconProps} />
            <TextInput placeholder={props.placeholder}
                       value={props.value}
                       onChangeText={props.onChangeText}
                       style={[SearchBarStyle.input, props.inputStyle]}
                       autoCapitalize={"none"}
                       caretHidden={false}
                       placeholderTextColor={"#55555588"}
                       selectionColor={"black"}
            />
        </View>
    );
}

const SearchBarStyle = StyleSheet.create({
    searchBar: {
        borderWidth: 1,
        borderColor: Colors.blue,
        alignItems: "center",
        justifyContent: "flex-start",
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 6
    },
    icon: {
        marginLeft: 4,
        marginRight: 8,
    },
    input: {
        flex: 1
    }
});

export default SearchBar;

export interface SearchBarProps {
    style?: StyleProp<ViewStyle>;
    value: string;
    placeholder?: string;
    onChangeText: (arg0: string) => void;
    iconStyle?: FontAwesomeIconStyle;
    iconProps?: Props;
    inputStyle?: StyleProp<TextStyle>

}