import {View} from "react-native";
import Logo from "../logo/Logo";
import {OpenTextTitle} from "../Texts";
import React from "react";

export default function HeadTitle(props: any) {
    return (
        <View style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
        }}>
            <Logo style={{
                marginRight: 24
            }}/>
            <OpenTextTitle>Local'ise</OpenTextTitle>
        </View>
    )
}