import React from "react";
import {SafeAreaView, ScrollView, ScrollViewProps, View, ViewProps, ViewStyle} from "react-native";

export default function Layout(props: { areaStyle?: ViewStyle} & ViewProps) {

    const { style, areaStyle, ...other} = props;

    return (
        <View style={[{flex: 1}, {paddingVertical: 16, paddingHorizontal: 12, backgroundColor: "white"}, style]} {...other}>
            { areaStyle ? (
                <View style={[{flex: 1}, areaStyle]}>
                    {props.children}
                </View>
            ) : (
                <>
                    {props.children}
                </>
            ) }
        </View>
    );
}

export function LayoutScrollable(props: ScrollViewProps) {
    return (
        <View style={{flex: 1}}>
            <ScrollView contentContainerStyle={[{flexGrow: 1}, props.contentContainerStyle]}
                        scrollEnabled={true}
                        persistentScrollbar={true}
                        nestedScrollEnabled={true}
                        {...props}
            >
                {props.children}
            </ScrollView>
        </View>
    );
}

