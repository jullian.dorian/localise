import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {Colors, Containers} from "../../constants/Utils";
import {faBookmark, faBars, faHome} from "@fortawesome/free-solid-svg-icons";
import {OpenText} from "../Texts";

const styles = StyleSheet.create({
    footer: {
        backgroundColor: Colors.blue,
        flex: 1,
        maxHeight: 80
    },
    container: {
        paddingHorizontal: Containers.padding,
        paddingVertical: 8,
        flex: 1,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: 'row',
        flexWrap:"nowrap",
    },
    button: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    icon: {
        color: Colors.white,
    },
    text: {
        color: Colors.white,
        textTransform: "uppercase",
        fontSize: 14,
        marginTop: 6,
        textAlign: "center",
    }
});

function Button({icon, text}: any) {
    return (
        <TouchableOpacity style={styles.button}>
            {/*    Icon*/}
            <FontAwesomeIcon icon={icon} size={24} style={styles.icon}/>
            {/*    Text*/}
            <OpenText style={styles.text}>{text}</OpenText>
        </TouchableOpacity>
    )
}

function Footer() {
    return (
        <View style={styles.footer}>
            <View style={styles.container}>
                <Button icon={faHome} text={"Accueil"}/>
                <Button icon={faBookmark} text={"Catalogue"}/>
                <Button icon={faBars} text={"Plus"}/>
            </View>
        </View>
    );
}

export default Footer;