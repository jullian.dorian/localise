import React from 'react';
import {StyleSheet, View, Text, Image, ImageComponent} from "react-native";
import {Colors, Containers} from "../../constants/Utils";
import {OpenText} from "../Texts";
import Logo from "../logo/Logo";

function Header() {

    return (
        <View style={styles.header}>
            <View style={styles.container}>
                <Logo style={styles.logo}/>
                <OpenText style={styles.title}>Local'Ise</OpenText>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        maxHeight: 100,
        backgroundColor: Colors.blue,
    },
    container: {
        flex: 1,
        height: 90,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        padding: Containers.padding,
    },
    logo: {
        width: 64,
        height: 64,
        resizeMode: "center"
    },
    title: {
        fontSize: 24,
        color: Colors.white
    }
});

export default Header;