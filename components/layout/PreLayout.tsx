import {View, ViewProps} from "react-native";

export default function PreLayout(props: ViewProps) {
    return (
        <View style={{
            flex: 1,
            justifyContent: "space-between",
            alignItems: "center",
            paddingVertical: 32,
            paddingHorizontal: 16,
            position: "relative"
        }} {...props}>
            {props.children}
        </View>
    )
}
