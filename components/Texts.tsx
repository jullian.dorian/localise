import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import React from "react";
import {Text, TextProps, View} from "react-native";
import {TextIconProps} from "../constants/types";

export function OpenText(props: TextProps) {
    return (
        <Text style={[{fontFamily: 'opensans', fontSize: 16, color: 'black', marginBottom: 8}, props.style]} {...props}>{props.children}</Text>
    )
}

export function OpenTextTitle(props: TextProps) {
    return (
        <Text  style={[{fontFamily: 'opensans', fontSize: 28}, props.style]} >{props.children}</Text>
    )
}

export function TextIcon(props: TextIconProps) {

    const showText = props.showText !== undefined ? props.showText : true;

    const HandleTimes = () => {
        if(props.times) {
            const t = [];
            for (let i = 0; i < props.times; i++) {
                t.push(i);
            }
            return (
                <>
                    {
                        t.map((i, index) => {
                            return  <FontAwesomeIcon style={[{marginLeft: 8}, props.iconStyle]} icon={props.icon} key={i}/>
                        })
                    }
                </>
            );
        }
        return <FontAwesomeIcon style={[{marginLeft: 8}, props.iconStyle]} icon={props.icon}  />;
    }

    return (
        <View style={[{flexDirection: "row", alignItems: "center", justifyContent: "center"}, props.viewStyle]} >
            {showText && <OpenText {...props.textContent}>{props.text}</OpenText> }
            <HandleTimes />
        </View>
    )
}
