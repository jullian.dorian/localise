import {View, StyleSheet, TouchableOpacity} from "react-native";
import {ButtonProps} from "../../constants/types";
import {OpenText} from "../Texts";

export default function ButtonSecondary (props : ButtonProps) {
    return (
        <View style={secondary.main}>
            <TouchableOpacity style={secondary.button} onPress={props.action} >
                <OpenText style={secondary.text}>
                    {props.text}
                </OpenText>
            </TouchableOpacity>
        </View>
    )
};

const secondary = StyleSheet.create({
    main: {
        alignSelf:"center",
        width: 360,
        overflow: 'hidden'
    },
    button: {
        paddingVertical: 12,
    },
    text: {
        fontSize: 15,
        textTransform: "uppercase",
        textAlign:"center",
        color: 'black'
    }
});