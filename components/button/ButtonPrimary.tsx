import {View, StyleSheet, TouchableOpacity, ActivityIndicator, StyleProp, ViewStyle} from "react-native";
import {ButtonProps} from "../../constants/types";
import {OpenText} from "../Texts";
import {Colors} from "../../constants/Utils";
import {useEffect, useState} from "react";

export default function ButtonPrimary (props : ButtonProps & { loading?: boolean, background?: string }) {

    /*const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        setLoading(props.loading??false);
    }, [props.loading])
*/
    function ChecksBackground(v?:string): StyleProp<ViewStyle> {

        switch (v) {
            case "red":
            case "RED":
            case RED:
                return [primary.main, primary.red]
            case "GREEN":
            case "green":
            case GREEN:
                return [primary.main, primary.green];
            default:
                return primary.main;
        }
    }

    return (
        <View style={ChecksBackground(props.background)}>
            <TouchableOpacity style={[primary.button, props.loading ? {opacity: 0.80} : {}]} onPress={props.action} disabled={props.loading} >
                {
                    props.loading ?
                        (
                            <ActivityIndicator size={"small"} color={"white"} />
                        ) :
                        (
                            <OpenText style={primary.text}>
                                {props.text}
                            </OpenText>
                        )
                }
            </TouchableOpacity>
        </View>
    )
}

const primary = StyleSheet.create({
    main: {
        backgroundColor: Colors.darkblue,
        borderRadius: 32,
        alignSelf:"stretch",
        marginVertical: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.35,
        shadowRadius: 6.27,
        elevation: 4,
    },
    button: {
        paddingVertical: 12,
    },
    text: {
        fontSize: 14,
        fontWeight: "bold",
        textTransform: "uppercase",
        textAlign:"center",
        color: 'white'
    },
    green: {
        backgroundColor: Colors.green,
    },
    red: {
        backgroundColor: Colors.red,
    }
});

export const GREEN = "green";
export const RED = "red";
