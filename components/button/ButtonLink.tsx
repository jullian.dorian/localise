import {GestureResponderEvent, StyleSheet, TouchableOpacity, View, ViewProps} from "react-native";
import {OpenText} from "../Texts";
import {Colors} from "../../constants/Utils";

export default function ButtonLink (props: ViewProps & {onClick: (args: GestureResponderEvent) => void}) {
    return (
        <View style={button.main}>
            <TouchableOpacity style={button.button} onPress={props.onClick} >
                <OpenText style={button.text}>
                    {props.children}
                </OpenText>
            </TouchableOpacity>
        </View>
    )
}

const button = StyleSheet.create({
    main: {
        alignSelf:"flex-end",
    },
    button: {
        paddingTop: 6
    },
    text: {
        textAlign: "center",
        fontSize: 14,
        textDecorationLine:"underline",
        color: Colors.black
    }
});
