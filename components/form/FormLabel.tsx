import {OpenText} from "../Texts";
import {TextProps} from "react-native";
import {Colors} from "../../constants/Utils";

export default function FormLabel(props: TextProps) {
    return (
        <OpenText {...props} style={{fontSize: 18, marginBottom: 8, color: Colors.black}} >{props.children}</OpenText>
    )
}
