import React, {useState} from 'react';
import {
    StyleProp,
    StyleSheet,
    TextInput,
    TextInputProps,
    TouchableOpacity,
    View,
    ViewProps,
    ViewStyle
} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import {Colors} from "../../constants/Utils";

export default function FormInput(props: TextInputProps & { passwordHide?: boolean, styleContent?: StyleProp<ViewStyle>}) {

    const [hidden, setHidden] = useState<boolean>(false);

    const togglePassword = () => {
        setHidden(!hidden);
    }

    const { secureTextEntry, style, styleContent, ...others } = props;

    if(props.passwordHide !== undefined && props.passwordHide) {
        return (
            <View style={[inputStyle.button, { flexDirection: "row", alignItems: "center", justifyContent: "space-between"}, styleContent]}>
                <TextInput style={[{flexGrow: 1, alignSelf: "stretch"}, style]} autoCapitalize={"none"} secureTextEntry={!hidden} {...others} selectionColor={"black"} />
                <TouchableOpacity style={{marginHorizontal: 8}} onPress={togglePassword}>
                    <FontAwesome name={hidden ? "eye-slash" : "eye"} size={24} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <TextInput style={[inputStyle.button, style]} autoCapitalize={"none"} {...others} selectionColor={"black"}/>
    );
}

const inputStyle = StyleSheet.create({
    button: {
        alignSelf: "stretch",
        marginVertical: 8,
        paddingVertical: 12,
        paddingHorizontal: 12,
        fontFamily: 'opensans',
        backgroundColor: 'white',
        overflow: "visible",
        textAlign: "left",
        borderRadius: 32,
        shadowColor: Colors.shadow,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.60,
        shadowRadius: 4.25,
        elevation: 5
    }
})
