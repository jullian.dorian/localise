import {
    KeyboardAvoidingView,
    KeyboardAvoidingViewProps,
    Platform,
    ScrollView,
    ScrollViewProps,
    StyleProp, ViewStyle
} from "react-native";

export default function Form(props: KeyboardAvoidingViewProps & { styleView?: StyleProp<ViewStyle>}) {

    const { style, styleView, ...others } = props;

    return (
        <KeyboardAvoidingView style={[{justifyContent: "center", flex:1, flexDirection: "column", paddingVertical: 16, alignSelf: "stretch"}, style]}
                              enabled={true}
                              behavior={Platform.OS === "ios" ? "position" : "height"}
        >
            <ScrollView  contentContainerStyle={[{flexGrow: 1, justifyContent: "center"}, styleView]} centerContent={true}>
                {props.children}
            </ScrollView>
        </KeyboardAvoidingView>
    )
}
