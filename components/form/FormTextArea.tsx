import React, {useState} from 'react';
import {StyleSheet, TextInput, TextInputProps, TouchableOpacity, View} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import {Colors} from "../../constants/Utils";

export default function FormInput(props: TextInputProps & { passwordHide?: boolean}) {

    const [hidden, setHidden] = useState<boolean>(false);

    if(props.passwordHide !== undefined && props.passwordHide) {
        return (
            <View>
                <TextInput style={style.button} autoCapitalize={"none"} {...props} />
                <TouchableOpacity style={{position: "absolute", right: 0, top: "50%", zIndex: 10}}>
                    <FontAwesome name={"home"} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <TextInput style={style.button} autoCapitalize={"none"} {...props} />
    );
}

const style = StyleSheet.create({
    button: {
        alignSelf: "stretch",
        marginVertical: 8,
        paddingVertical: 12,
        paddingHorizontal: 12,
        fontFamily: 'opensans',
        backgroundColor: 'white',
        textAlign: "center",
        borderRadius: 32,
        shadowColor: Colors.shadow,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.60,
        shadowRadius: 4.25,
        elevation: 4
    }
})
