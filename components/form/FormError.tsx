import {OpenText} from "../Texts";
import {Colors} from "../../constants/Utils";
import {FormErrorProps} from "../../constants/types";
import {useEffect, useState} from "react";
import {Pressable} from "react-native";

export default function FormError(props: FormErrorProps) {

    const [message, setMessage] = useState<string>("");

    useEffect(() => {
        setMessage("");
        if(props.errors) {
            for (let error of props.errors) {
                if (error.field === props.field) {
                    setMessage(error.message);
                    break;
                }
            }
        }
    }, [props.errors]);

    return (
        <Pressable onPress={() => {
            if(message) {
                setMessage("");
            }
        }}>
            <OpenText {...props} style={{fontSize: 12, color: Colors.red}} >{message}</OpenText>
        </Pressable>
    )
}
