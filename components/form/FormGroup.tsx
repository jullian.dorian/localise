import {View, ViewProps} from "react-native";

export default function FormGroup (props: FormGroupProps) {

    const { style, children, ...other} = props;

    return (
        <View style={[
            {
                flex: 0,
                marginVertical: 12,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "stretch",
            }, style]
        } {...other}>
            {children}
        </View>
    )
}

export interface FormGroupProps extends ViewProps{

}
