import {Animated, Easing, Image, View} from "react-native";
import {LogoProps} from "../../constants/types";
import {useEffect, useRef, useState} from "react";

export default function Logo(props: LogoProps) {

    return (
        <Image source={require('../../assets/logo-localise.png')} resizeMode={"contain"} style={{width: props.size??92, height: props.size??92, ...props.style}}/>
    )
}

export function AnimatedLogo(props: LogoProps) {
    const maxValue = 32;
    const minValue = 28;
    const popAnim = new Animated.Value(0);
    const [size, setSize] = useState<number>(props.size??92);

    useEffect(()=> {
        popAnim.setValue(0);

        popAnim.addListener((v) => {
            setSize(size + v.value);
        });

        Animated.loop(
            Animated.sequence([
                Animated.timing(popAnim, {
                    toValue: maxValue,
                    duration: 100,
                    useNativeDriver: false
                }),
                Animated.timing(popAnim, {
                    toValue: minValue - 4,
                    duration: 100,
                    useNativeDriver: false
                }),
                Animated.timing(popAnim, {
                    toValue: maxValue + 6,
                    duration: 100,
                    useNativeDriver: false
                }),
                Animated.timing(popAnim, {
                    toValue: minValue,
                    duration: 1000,
                    useNativeDriver: false
                })
            ])
        ).start();

        return popAnim.removeAllListeners();
    }, [popAnim]);

    return (
        <Animated.View>
            <Logo {...props} size={size}/>
        </Animated.View>
    )
}