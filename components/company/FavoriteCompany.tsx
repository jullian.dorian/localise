import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, {useEffect, useState} from 'react';
import {Platform, Pressable, StyleSheet, ToastAndroid, View} from "react-native";
import {CompanyProps} from "../../constants/types";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {faStar as faStarRegular} from "@fortawesome/free-regular-svg-icons";
import {Colors} from "../../constants/Utils";
import {instance} from "../../auth/uri";

function FavoriteCompany({ company }: { company: CompanyProps }) {

    const [added, setAdded] = useState<boolean>(false);

    const handleFavorite = async () => {
        await instance.post("companies/"+company.id+"/favorite", { favorite: !added})
            .then((res) => {
                setAdded(res.data.isFavorite);
                const message = added ? "Ajouté aux favoris !" : "Supprimé des favoris !";

                if(Platform.OS !== "ios")
                    ToastAndroid.show(message, ToastAndroid.SHORT);
            })
            .catch((err) => {
                if(Platform.OS !== "ios")
                    ToastAndroid.show("Impossible de modifier les favoris.", ToastAndroid.SHORT);
            });
    }

    useEffect(() => {
        (async () => {
            await instance.get("companies/"+company.id+"/favorite")
                .then((res) => {
                    setAdded(res.data.isFavorite);
                })
                .catch((err) => {
                    console.log(err);
                })
        })();
    }, []);

    return (
        <Pressable style={FavoriteCompanyStyle.button} onPress={handleFavorite}>
            {
                added ?
                    (
                        <FontAwesomeIcon icon={faStar} size={32} color={Colors.gold}/>
                    )
                    :
                    (
                        <FontAwesomeIcon icon={faStarRegular} size={32} color={Colors.gold} />
                    )
            }
        </Pressable>
    );
}

const FavoriteCompanyStyle = StyleSheet.create({
    button: {
        position: "absolute",
        zIndex: 999,
        right: 16,
        top: 16
    }
});

export default FavoriteCompany;