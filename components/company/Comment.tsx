import {Image, StyleSheet, View, ViewProps} from "react-native";
import {OpenText} from "../Texts";
import {Colors, Utilities} from "../../constants/Utils";
import {baseURL} from "../../auth/uri";
import {CommentProps} from "../../constants/types";

export default function Comment(props: ViewProps & { comment: CommentProps}) {
    return (
        <View style={style.content} {...props}>
          <View style={style.header}>
            <Image style={style.picture} source={{uri: baseURL + "users/picture/"+props.comment.user.picture}} />
            <View style={style.info}>
              <OpenText style={style.pseudo}>{props.comment.user.pseudo}</OpenText>
              <OpenText style={style.date}>{Utilities.parseDate(props.comment.createdAt)}</OpenText>
            </View>
          </View>
          <OpenText>{props.comment.message}</OpenText>
        </View>
    );
}

const style = StyleSheet.create({
  content: {
    borderRadius: 8,
    backgroundColor: Colors.blueTransparency,
    paddingVertical: 8,
    paddingHorizontal: 8,
    marginBottom: 16,
    position: "relative"
  },
  header: {
    flexDirection: "row",
    alignItems: "center"
  },
  picture: {
    backgroundColor: 'white',
    width: 64,
    height: 64,
    borderRadius: 64,
  },
  pseudo: {
    fontSize: 16,
    fontWeight: "bold"
  },
  date: {
    fontSize: 11
  },
  info: {
    marginLeft: 8
  }
});
