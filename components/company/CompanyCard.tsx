import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Dimensions,
    TouchableOpacity,
    ListRenderItemInfo
} from "react-native";
import {faLocationDot} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {Utilities} from "../../constants/Utils";
import {baseURL} from "../../auth/uri";
import {CompanyProps} from "../../constants/types";

function CompanyCard({ item, navigation } : (ListRenderItemInfo<CompanyProps> | { item : CompanyProps}) & { navigation : any}) {

    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);
    const imageWidth = dimensions.width;

    return (
        <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('Company', { company: item })}>
            <Image source={{uri: baseURL + "companies/" + item.id + "/picture"}} style={[{width: imageWidth, height: imageHeight},styles.picture]}/>
            <Text style={styles.name}>{item.name}</Text>
            <View style={styles.data}>
                <FontAwesomeIcon icon={faLocationDot} />
                <Text style={styles.distance}>{Utilities.distanceCompany(0,0)}</Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        // backgroundColor: 'rgba(50,50,50,0.05)',
        paddingBottom: 8,
        marginBottom: 8,
        borderBottomColor: "black",
        borderBottomWidth: 1,
    },
    picture: {
        flex: 0,
        resizeMode: 'cover',
    },
    name: {
        fontSize: 16,
        fontWeight: "bold",
        paddingTop: 6
    },
    data: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 12,
        position: 'absolute',
        right: 8,
        top: 8,
        backgroundColor: 'white',
        borderRadius: 24,
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderColor: 'black',
        borderWidth: 2
    },
    distance: {
        paddingLeft: 4
    }
});

export default CompanyCard;
