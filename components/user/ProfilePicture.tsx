import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Image, TouchableOpacity} from "react-native";
import {Colors} from "../../constants/Utils";
import * as ImagePicker from "expo-image-picker";
import {baseURL, instance} from "../../auth/uri";
import {AxiosError} from "axios";
import {useUser} from "../../hooks/useCachedResources";

function ProfilePicture(props: any) {

    const [loadingUserComplete, user, setUser] = useUser();
    const [picture, setPicture] = useState<string|null|undefined>(null);

    const pickImageLibrary = async () => {
        await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            allowsMultipleSelection: false,
            aspect: [4,6],
            quality: 1,
            base64: false,
        }).then((result)=> {
                if(!result.cancelled) {
                    const upload: any = {
                        uri: result.uri,
                        name: "upload",
                        type: "image/jpg",
                    }

                    const f: FormData = new FormData();
                    f.append("picture", upload);
                    instance.post("users/picture", f, { headers: {'Content-Type': 'multipart/form-data'}, transformRequest: (data: any, headers: any) => data, responseEncoding: "utf-8" })
                        .then(async (result) => {
                            if(user) {
                                await setUser({...user, picture: result.data});
                                setPicture(result.data);
                            }
                        })
                        .catch((error: AxiosError) => {
                            console.log(error.message);
                        });
                }
            })
    }

    useEffect(() => {
        (async () => {
            await ImagePicker.requestMediaLibraryPermissionsAsync();
        })();
    }, [])

    useEffect(() => {
        if(user)
            setPicture(user?.picture);
    }, [loadingUserComplete, user]);

    return (
        <TouchableOpacity style={ProfilePictureStyle.profile} onPress={pickImageLibrary}>
            {
                loadingUserComplete && picture && <Image source={{uri: baseURL + "users/picture/" + picture, cache: "reload"}} style={ProfilePictureStyle.picture} resizeMode={"cover"} key={picture} />
            }
        </TouchableOpacity>
    );
}

const PictureSize = 164;

const ProfilePictureStyle = StyleSheet.create({
    profile: {
        backgroundColor: Colors.white,
        borderRadius: PictureSize,
        alignSelf: "center",
        borderColor: Colors.blue,
        borderWidth: 4,
        shadowColor: Colors.shadow,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        overflow: "hidden",
        width: PictureSize,
        height: PictureSize
    },
    picture: {
        width: PictureSize,
        height: PictureSize,
    }
});

export default ProfilePicture;
