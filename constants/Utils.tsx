export const Colors = {
    blue: '#81cee5',
    white: '#ffffff',
    black: '#111111',
    shadow: 'rgba(30,30,30,0.4)',
    darkblue: 'rgb(65,157,177)',
    red: 'rgb(200,100,100)',
    green: 'rgb(78,219,145)',
    blueTransparency: 'rgba(129,206,229,0.4)',
    gold: 'rgb(224,187,37)'
}

export const Containers = {
    padding: 16
}


export class Utilities {

    static distanceCompany(position: any, company: any): string {
        return "0 m";
    }

    static parseDate(date: Date) {
        const d = new Date(date);
        return d.toLocaleDateString() + " " + d.toLocaleTimeString();
    }

    static toRadians(degree: number) {
        return degree * (Math.PI / 180);
    }

    static calculateDistanceDegree(start: any & { latitude: number, longitude: number}, end: any & { latitude: number, longitude: number}) {
        const a = this.toRadians(start.latitude);
        const b = this.toRadians(end.latitude);
        const y = Math.acos(Math.sin(a) * Math.sin(b) + Math.cos(a) * Math.cos(b) * Math.cos(this.toRadians(start.longitude - end.longitude))) * 6371;
        const r = Math.round(y * 100) / 100;
        return r + " km";
    }
}
