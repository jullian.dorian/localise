import {AccessibilityProps, ImageStyle, StyleProp, TextProps, ViewProps, ViewStyle} from "react-native";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIconStyle, Props} from "@fortawesome/react-native-fontawesome";

export type ButtonProps = {
    text: string,
    action?: any,
}

export interface LogoProps extends AccessibilityProps {
    size?: number,
    style?: ImageStyle
}

export interface FormErrorProps extends TextProps {
    errors?: ErrorProps[],
    field: string
}

export interface ErrorProps {
    field: string,
    rule: string,
    message: string
}

export interface TextIconProps {

    text?: string,
    icon: IconProp,
    iconStyle?: FontAwesomeIconStyle,
    textContent?: TextProps,
    viewStyle?: StyleProp<ViewStyle>,
    reverse?: boolean,
    times?: number;
    showText?: boolean
}

export interface CategoryProps {
    name: string,
    id: number,
    picture: string|null
}

export interface CompanyProps {
    id: number,
    name: string,
    description: string,
    uri: string,
    latitude: number,
    longitude: number
}

export interface CommentProps {
    user: UserCommentProps,
    message: string,
    createdAt: Date
}

export interface UserCommentProps {
    id: number,
    pseudo: string,
    picture: string
}
