import React, {useEffect, useMemo, useState} from 'react';
import {StyleSheet} from "react-native";
import {NavigationContainer, RouteProp} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import LoginScreen from "../screens/auth/LoginScreen";
import RegisterScreen from "../screens/auth/RegisterScreen";
import {AuthContext, AuthProps} from "../auth/auth";
import MainScreen from "../screens/MainScreen";
import * as SecureStore from "expo-secure-store";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {FontAwesome} from "@expo/vector-icons";
import {Colors} from "../constants/Utils";
import ForgotPasswordScreen from "../screens/auth/ForgotPasswordScreen";
import CompaniesScreen from "../screens/company/CompaniesScreen";
import {OpenText} from "../components/Texts";
import CompanyProfileScreen from "../screens/company/CompanyProfileScreen";
import AccountScreen from "../screens/profile/AccountScreen";
import LoadingScreen from "../screens/LoadingScreen";
import {QueryClient, QueryClientProvider} from 'react-query';
import {instance, instanceAuth} from "../auth/uri";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {AxiosRequestConfig} from "axios";
import ChangePasswordScreen from "../screens/profile/ChangePasswordScreen";


const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const queryClient = new QueryClient();

function Navigator() {

    const [auth, setAuth] = useState<boolean>();
    const [loading, setLoading] = useState<boolean>(true);
    const [interceptor, setInterceptor] = useState(0);

    const intercept = (token: string, config: AxiosRequestConfig) => {
        if(config && config.headers) {
            config.headers.Authorization = "Bearer " + token;
        }
        return config;
    }

    const authContext = useMemo(() => ({
        signIn: async (data: AuthProps) => {
            try {
                await SecureStore.setItemAsync("token", data.token);
                await AsyncStorage.setItem("user", JSON.stringify(data.user));
                setAuth(true);

                setInterceptor(instance.interceptors.request.use(config => intercept(data.token, config)))
            } catch (err) {
                console.log(err);
            }
        },
        signOut: async () => {
            await SecureStore.deleteItemAsync("token");
            await AsyncStorage.clear();
            setAuth(false);
            instance.interceptors.request.eject(interceptor);
        },
        token: async () => {
            return await SecureStore.getItemAsync("token");
        },
    }), []);

    useEffect(() => {
        setLoading(true);
        (async () => {
            let token: string | null = null;
            try {
                token = await SecureStore.getItemAsync("token");
            } catch (e) {
                console.log(e);
            }

            if(interceptor) {
                instance.interceptors.request.eject(interceptor);
            }

            setAuth(false);

            if(token) {
                await instanceAuth.get("ping", { headers: { 'Authorization': 'Bearer ' + token}}).then((res) => {
                    setInterceptor(instance.interceptors.request.use((config) => token ? intercept(token, config) : config));
                    setAuth(true);
                }).catch((err) => {
                    setAuth(false)
                })
            }
            setLoading(false);
            setAuth(true);
        })();
    }, []);

    if(loading) {
        return <LoadingScreen />
    }

    return (
        <AuthContext.Provider value={authContext}>
            <QueryClientProvider client={queryClient}>
                <NavigationContainer>
                    {
                        !auth ?
                            (
                                <Stack.Navigator screenOptions={{headerShown:false}}>
                                    <Stack.Screen name="Login" component={LoginScreen} />
                                    <Stack.Screen name="Register" component={RegisterScreen} />
                                    <Stack.Screen name="Forgot" component={ForgotPasswordScreen} />
                                </Stack.Navigator>
                            ) :
                            (
                                <BottomNavigation />
                            )
                        }

                </NavigationContainer>
            </QueryClientProvider>
        </AuthContext.Provider>
    );
}

function BottomNavigation() {
    return (
        <Tab.Navigator screenOptions={{
            headerShown: false,
            tabBarStyle: {
                backgroundColor: Colors.blue,
            },
            tabBarIconStyle: {
                color: "#e5e5e5",
            },
            tabBarActiveTintColor: Colors.white,
            tabBarInactiveTintColor: Colors.shadow,
            tabBarHideOnKeyboard: true,
        }}>
            <Tab.Screen name="Home" component={CategoryNavigation} options={{
                tabBarLabel: "Accueil",
                tabBarIcon: ({color}) => (
                    <FontAwesome name={"home"} color={color} size={24} />
                ),

            }}/>
            <Tab.Screen name="Account" component={ProfileNavigation} options={{
                tabBarLabel: "Profile",
                tabBarIcon: ({color}) => (
                    <FontAwesome name={"user"} color={color} size={24} />
                ),

            }}/>
        </Tab.Navigator>
    )
}

function CategoryNavigation() {
    return (
        <Stack.Navigator initialRouteName={"Main"} screenOptions={{
            headerShown: true,
            headerStyle: {
                backgroundColor: Colors.blue
            },
            headerTintColor: Colors.white
        }}>
            <Stack.Screen name="Main" component={MainScreen} options={{headerShown: false}}/>
            <Stack.Screen name="Companies" component={CompaniesScreen} options={({route}: {route: RouteProp<any>}) => ({headerTitle: (props) => computeTitleNavigator(route)})} />
            <Stack.Screen name="Company" component={CompanyProfileScreen} options={({route}: {route: RouteProp<any>}) => ({headerTitle: (props) => computeTitleNavigator(route)})} />
        </Stack.Navigator>

    )
}

function ProfileNavigation() {
    return (
        <Stack.Navigator initialRouteName={"Main"} screenOptions={{
            headerShown: true,
            headerStyle: {
                backgroundColor: Colors.blue
            },
            headerTintColor: Colors.white
        }}>
            <Stack.Screen name="Main" component={AccountScreen} options={{headerShown: false}}/>
            <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} options={{headerTitle: "Paramètres"}}/>
        </Stack.Navigator>

    )
}

function computeTitleNavigator(route: RouteProp<any>) {
    return (
        <OpenText style={{textTransform: "uppercase", fontSize: 18, color: Colors.white}}>{route.params?.title}</OpenText>
    )
}

export default Navigator;
