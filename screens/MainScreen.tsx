import {FlatList, Image, SafeAreaView, ScrollView, TouchableOpacity, View} from "react-native";
import Layout from "../components/layout/Layout";
import {OpenText} from "../components/Texts";
import {useContext, useState} from "react";
import SearchBar from "../components/SearchBar";
import axios, {AxiosResponse} from "axios";
import {baseURL, genHeaders, instance} from "../auth/uri";
import {AuthContext} from "../auth/auth";
import {CategoryProps} from "../constants/types";
import {useQuery} from "react-query";

export default function MainScreen(props: any) {

    const [search, setSearch] = useState<string>("");
    const { token } = useContext(AuthContext);

    const fetchCategoriesByName = async (s: string) => {
        return await instance.get("categories/search/" + s.toLowerCase(), genHeaders(await token())).then((res) => {
            return res.data;
        }).catch((error) => console.log(error));
    }

    const { data, status } = useQuery<CategoryProps[]>(['categories', search], () => fetchCategoriesByName(search), { initialData: [{id: -1, name: "test", picture: null}]});

    const renderCategory = ({ item } : { item: CategoryProps }): any => {
        return (
            <View style={{ flex: 1, marginTop: 8,  minHeight: 140, backgroundColor: 'black'}}>
                <TouchableOpacity style={{flex: 1, position: "relative", justifyContent: 'center', alignItems: 'center'}} onPress={() => {props.navigation.navigate("Companies", { id: item.id, title: item.name })}}>
                    <View style={{position: "absolute", width: "100%", height: "100%"}}>
                        <Image style={{opacity: 0.7,width: "100%", height: "100%", resizeMode: "cover"}}
                               source={{uri: baseURL + "categories/" + item.id + "/picture"}} />
                    </View>
                    <OpenText style={{color: 'white', textAlign: 'center', fontSize: 16, fontWeight:'bold', textTransform: 'uppercase'}}>{item.name}</OpenText>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <Layout>
            <SearchBar placeholder={"Chercher une catégorie"}
                       value={search}
                       onChangeText={setSearch}
            />
            {
                status === "error" || (data?.length === 0) ?
                    (
                        <View>
                            <OpenText>Nous ne trouvons aucune catégorie de ce nom !</OpenText>
                        </View>
                    )
                    :
                    (
                        <SafeAreaView style={{flex: 1,paddingBottom: 8}}>
                            <FlatList data={data}
                                      renderItem={renderCategory}
                                      keyExtractor={(item,index) => ""+index}
                                      contentContainerStyle={{justifyContent: "space-between"}}

                            />
                        </SafeAreaView>
                    )
            }

        </Layout>
    )
}
