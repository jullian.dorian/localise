import React, {useLayoutEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, Text, View} from "react-native";
import Layout from "../../components/layout/Layout";
import SearchBar from "../../components/SearchBar";
import CompanyCard from "../../components/company/CompanyCard";
import {instance} from "../../auth/uri";
import {useQuery} from "react-query";
import {OpenText} from "../../components/Texts";
import {CompanyProps} from "../../constants/types";

function CompaniesScreen(props: { navigation: any , route: any }) {

    const [search, setSearch] = useState<string>("");
    const { id, title } = props.route.params;

    const fetchCompaniesByCategory = async (name: string) => {
        return await instance.get("companies/category/"+id+"/"+name.toLowerCase()).then((res) => {
            return res.data;
        }).catch((error) => console.log(error));
    }

    const { data, status } = useQuery<CompanyProps[]>(['companies', search], () => fetchCompaniesByCategory(search), { initialData: [{id: -1, name: "test", description: "", uri: "oui.png", latitude: 43.6399489701029, longitude: 3.8289022522228446}]});

    useLayoutEffect(() => {
        props.navigation.setParams({ title })
    }, []);

    return (
        <Layout>
            <SearchBar value={search} onChangeText={setSearch} placeholder={"Chercher un nom, plat, etc..."}/>
            {
                status === "loading" ?
                    (
                        <OpenText>Chargement</OpenText>
                    )
                    :
                    (
                        <SafeAreaView style={CompaniesScreenStyle.container}>
                            <FlatList data={data}
                                      renderItem={({item}: { item: CompanyProps}) => <CompanyCard item={item} navigation={props.navigation}/>}
                                      keyExtractor={(item,index) => ""+index}
                            />
                        </SafeAreaView>
                    )
            }
        </Layout>
    );
}

const CompaniesScreenStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 8,
        paddingVertical: 8
    }
});

export default CompaniesScreen;
