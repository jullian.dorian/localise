import React, {useEffect, useLayoutEffect, useState} from 'react';
import {Dimensions, Image, StyleSheet, View} from "react-native";
import Layout, {LayoutScrollable} from "../../components/layout/Layout";
import {OpenText, TextIcon} from "../../components/Texts";
import {faMapLocation, faEuro, faUserAlt} from "@fortawesome/free-solid-svg-icons";
import ButtonPrimary from "../../components/button/ButtonPrimary";
import {baseURL, genHeaders, instance} from "../../auth/uri";
import {useMutation, useQuery, useQueryClient} from "react-query";
import Comment from "../../components/company/Comment";
import {CommentProps, CompanyProps} from "../../constants/types";
import FormInput from "../../components/form/FormInput";
import Form from "../../components/form/Form";
import {Colors, Utilities} from "../../constants/Utils";
import FavoriteCompany from "../../components/company/FavoriteCompany";
import * as Location from "expo-location";
import {LocationObject} from "expo-location/src/Location.types";
import openMap from "react-native-open-maps"

function CompanyProfileScreen(props: { navigation: any , route: any }) {

    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);

    const [comment, setComment] = useState<string|undefined>();
    const [loadingComment,setLoadingComment] = useState<boolean>(false);
    const [userLocation, setUserLocation] = useState<LocationObject>();
    const queryClient = useQueryClient();

    const companyClient: CompanyProps = props.route.params.company;

    const toggleMap = async () => {
        if(userLocation) {
            const query = `${companyClient.latitude},${companyClient.longitude}`;
            await openMap({ query: query, zoom: 18, travelType: "public_transport" });
        }
    }

    //récupération des commentaires
    const fetchComments = async () => {
        return await instance.get("companies/"+companyClient.id+"/comments").then((res) => {
            return res.data;
        }).catch((error) => {
            throw new Error(error)
        });
    }

    //post commentaire
    const postComments = async (message: string|undefined) => {
        return await instance.post("companies/" + companyClient.id + "/comments", { message }, { timeout: 4000 })
            .then((res) => {
                const { company, ...comments} = res.data;
                return comments;
            });
    }

    //Mutation des commentaires
    const mutationComments = useMutation(postComments, {
        onSuccess: async (data, variables) => {
            queryClient.setQueryData(["comments", companyClient.id], (old: any) => [...old, data]);
            setComment(undefined);
        }
    })

    //On récupère nos infos
    const { data, isSuccess, isLoading, isFetching  } = useQuery<CommentProps[]>(["comments", companyClient.id], () => fetchComments());

    //envoie de commentaire avec etats de chargement
    const postComment = async () => {
        setLoadingComment(true);
        await mutationComments.mutateAsync(comment);
        setLoadingComment(false);
    }

    useLayoutEffect(() => {
        props.navigation.setParams({ title: companyClient.name })
    }, []);

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                return;
            }

            const location = await Location.getCurrentPositionAsync({ timeInterval: 2000 });
            setUserLocation(location);
        })();
    }, []);

    return (
        <LayoutScrollable style={{paddingVertical: 0, paddingHorizontal: 0}} >
            {/*<FavoriteCompany company={companyClient} />*/}
            <Image source={{uri: baseURL + "companies/" + companyClient.id + "/picture"}} style={[{width: "100%", height: imageHeight}, CompanyProfileStyle.picture]} />
            <Layout>
                <OpenText style={CompanyProfileStyle.title}>{companyClient.name}</OpenText>
                <View style={CompanyProfileStyle.indicators}>
                    <TextIcon text={!userLocation ? "Calcul..." : Utilities.calculateDistanceDegree(userLocation.coords, { latitude: companyClient.latitude, longitude: companyClient.longitude})} icon={faMapLocation} viewStyle={{flex: 1}} reverse={true}/>
                    <TextIcon showText={false} times={2} icon={faEuro} viewStyle={{flex: 1}} reverse={true}/>
                    <TextIcon showText={false} times={3} icon={faUserAlt} viewStyle={{flex: 1}} reverse={true}/>
                </View>
                <OpenText style={CompanyProfileStyle.description}>{companyClient.description}</OpenText>
                <ButtonPrimary text={"Ouvrir la carte"} action={toggleMap} />
                <View style={CommentaryStyle.content}>
                    <OpenText style={CommentaryStyle.title}>Commentaires - {(data as CommentProps[])?.length||0}</OpenText>
                    <Form styleView={{paddingHorizontal: 8, paddingVertical: 0}}>
                        {!mutationComments.isIdle &&
                            <View style={{marginVertical: 6}}>
                                {mutationComments.isError &&
                                    <OpenText style={{color: Colors.green}}>Impossible d'ajouter le
                                        commentaire.</OpenText>}
                                {mutationComments.isSuccess &&
                                    <OpenText style={{color: Colors.green}}>Votre commentaire a bien été
                                        ajouté.</OpenText>}
                            </View>
                        }
                        <FormInput value={comment} onChangeText={setComment}
                                   style={{marginTop: 0, borderRadius: 24, elevation: 8}}
                                   maxLength={250}
                                   multiline={true}
                                   placeholder={"Le café était très bon..."}
                                   enablesReturnKeyAutomatically={false}
                                   keyboardType={"default"}
                                   editable={!loadingComment}
                        />
                        <ButtonPrimary text={"Ajouter un commentaire"} action={postComment} loading={loadingComment} />
                    </Form>
                    {
                        isLoading || isFetching ?
                            (
                                <OpenText>Chargement en cours</OpenText>
                            ) :
                            isSuccess && (
                                (data as CommentProps[]).map((item, key) => {
                                    return <Comment comment={item} key={key}/>
                                }).reverse()
                            )
                    }
                </View>
            </Layout>
        </LayoutScrollable>
    );
}

const CompanyProfileStyle = StyleSheet.create({
    picture: {
        backgroundColor: "gray"
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        paddingTop: 6,
        alignSelf: "center"
    },
    description: {
        fontSize: 14,
        alignSelf: "center",
        paddingVertical: 18,
        textAlign: "center"
    },
    indicators: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 12
    }
});
const CommentaryStyle = StyleSheet.create({
    content: {
        marginTop: 24
    },
    title: {
        fontSize: 19,
        paddingBottom: 4,
        marginBottom: 16,
        borderColor: 'black',
        borderBottomWidth: 1
    }
})

export default CompanyProfileScreen;