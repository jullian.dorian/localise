import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, StyleSheet, View} from "react-native";
import FormInput from "../../components/form/FormInput";
import ButtonPrimary from "../../components/button/ButtonPrimary";
import ButtonSecondary from "../../components/button/ButtonSecondary";
import Form from "../../components/form/Form";
import FormGroup from "../../components/form/FormGroup";
import FormLabel from "../../components/form/FormLabel";
import ButtonLink from "../../components/button/ButtonLink";
import PreLayout from "../../components/layout/PreLayout";
import HeadTitle from "../../components/layout/HeadTitle";
import FormError from "../../components/form/FormError";
import {AuthContext, AuthProps} from "../../auth/auth";
import {ErrorProps, FormErrorProps} from "../../constants/types";
import {instanceAuth} from "../../auth/uri";
import {NavigationProp} from "@react-navigation/native";
import {AxiosResponse} from "axios";

function LoginScreen({navigation}: {navigation: NavigationProp<any>}) {

    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const [errors, setErrors] = useState<ErrorProps[]>();

    const [verify, setVerify] = useState<boolean>(false);

    const { signIn } = useContext(AuthContext);

    useEffect(() => {
        setErrors([]);
        setVerify(false);

        return clearScreen();
    }, []);

    const clearScreen = () => {
        setErrors(undefined);
        setVerify(false);
        setEmail("");
        setPassword("");
    }

    const handleSubmit = async () => {
        setErrors([]);
        setVerify(true);

        await instanceAuth.post('login', { email, password })
            .then((result: AxiosResponse<AuthProps>) => {
                //On récupère les data et on fait les actions à faire
                signIn(result.data);
            })
            .catch((error) => {
                if(error.response) {
                    const data = error.response.data;
                    if(data.errors) {
                        setErrors(data.errors);
                    }
                }
            });

        setVerify(false);
    }

    return (
        <PreLayout>
            <HeadTitle />
            <Form>
                <FormGroup>
                    <FormLabel style={styles.label}>Adresse mail</FormLabel>
                    <FormError errors={errors} field={"email"}/>
                    <FormInput placeholder="albe59" keyboardType={"email-address"} onChangeText={setEmail}/>
                </FormGroup>
                <FormGroup>
                    <FormLabel style={styles.label}>Mot de passe</FormLabel>
                    <FormError errors={errors} field={"password"}/>
                    <FormInput placeholder="98945" secureTextEntry={true} onChangeText={setPassword} passwordHide/>
                    <ButtonLink onClick={(e) => navigation.navigate("Forgot")} >Mot de passe oublié ?</ButtonLink>
                </FormGroup>
            </Form>
            <View style={styles.foot}>
                <ButtonPrimary text={"Se connecter"} action={() => handleSubmit()} loading={verify}/>
                <ButtonSecondary text={"Je crée mon compte"} action={() => navigation.navigate('Register') }/>
            </View>
        </PreLayout>
    );
}

const styles = StyleSheet.create({
    label: {
        fontWeight: "bold",
        textTransform: "uppercase"
    },
    foot: {
        alignSelf: "stretch"
    }
});

export default LoginScreen;
