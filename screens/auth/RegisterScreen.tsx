import React, {useContext, useEffect, useState} from 'react';
import {KeyboardAvoidingView, Platform, ScrollView, StyleSheet, View} from "react-native";
import Form from "../../components/form/Form";
import FormGroup from "../../components/form/FormGroup";
import FormLabel from "../../components/form/FormLabel";
import FormInput from "../../components/form/FormInput";
import ButtonPrimary from "../../components/button/ButtonPrimary";
import ButtonSecondary from "../../components/button/ButtonSecondary";
import PreLayout from "../../components/layout/PreLayout";
import HeadTitle from "../../components/layout/HeadTitle";
import axios, {AxiosResponse} from "axios";
import FormError from "../../components/form/FormError";
import {ErrorProps} from "../../constants/types";
import {AuthContext, AuthProps} from "../../auth/auth";
import {instanceAuth} from "../../auth/uri";

function RegisterScreen({navigation}: any) {

    const [pseudo, setPseudo] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const [errors, setErrors] = useState<ErrorProps[]>([]);

    const [verify, setVerify] = useState<boolean>(false);

    const { signIn } = useContext(AuthContext);

    const handleSubmit = async () => {
        setErrors([]);
        setVerify(true);

        await instanceAuth.post('register', {pseudo, email, password})
            .then((result: AxiosResponse<AuthProps>) => {
                //On récupère les data et on fait les actions à faire
                signIn(result.data);
            })
            .catch((error) => {
                if(error.response) {
                    const data = error.response.data;
                    if(data.errors) {
                        setErrors(data.errors);
                    }

                }
            });

        setVerify(false);
    }

    return (
        <PreLayout>
            <HeadTitle />
            <Form>
                <FormGroup>
                    <FormLabel style={styles.label}>Pseudonyme</FormLabel>
                    <FormError errors={errors} field={"pseudo"}/>
                    <FormInput placeholder="albe59" onChangeText={setPseudo} />
                </FormGroup>
                <FormGroup>
                    <FormLabel style={styles.label}>Adresse mail</FormLabel>
                    <FormError errors={errors} field={"email"}/>
                    <FormInput placeholder="albe@gmail.com" keyboardType={"email-address"} onChangeText={setEmail}/>
                </FormGroup>
                <FormGroup>
                    <FormLabel style={styles.label}>Mot de passe</FormLabel>
                    <FormError errors={errors} field={"password"}/>
                    <FormInput placeholder="98945" secureTextEntry={true} onChangeText={setPassword} passwordHide/>
                </FormGroup>
            </Form>
            <View style={{alignSelf: "stretch"}}>
                <ButtonPrimary text={"Je crée mon compte"} action={() => handleSubmit()} loading={verify}/>
                <ButtonSecondary text={"J'ai deja un compte"} action={() => navigation.navigate('Login')}/>
            </View>
        </PreLayout>
    );
}
const styles = StyleSheet.create({
    label: {
        fontWeight: "bold",
        textTransform: "uppercase"
    }
});

export default RegisterScreen;
