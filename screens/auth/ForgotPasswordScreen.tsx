import {StyleSheet, Text, View} from "react-native";
import PreLayout from "../../components/layout/PreLayout";
import HeadTitle from "../../components/layout/HeadTitle";
import React, {useState} from "react";
import {OpenText} from "../../components/Texts";
import Form from "../../components/form/Form";
import FormGroup from "../../components/form/FormGroup";
import FormLabel from "../../components/form/FormLabel";
import FormError from "../../components/form/FormError";
import FormInput from "../../components/form/FormInput";
import ButtonPrimary, {GREEN} from "../../components/button/ButtonPrimary";
import ButtonSecondary from "../../components/button/ButtonSecondary";

export default function ForgotPasswordScreen(props: any) {

    const [step, setStep] = useState<ForgotStep>(EmailStep);

    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [code, setCode] = useState<string>("");

    function ChangePassword() {

    }

    function ShowStep() {
      switch (step) {
        case CodeStep:
          return <CodeScreen code={code} setCode={setCode} onBack={() => setStep(EmailStep)} onContinue={() => setStep(PasswordStep)} />
        case PasswordStep:
          return <PasswordScreen password={password} setPassword={setPassword} onBack={() => props.navigation.goBack()} onContinue={ChangePassword} />
        default:
          return <EmailScreen email={email} setEmail={setEmail} onBack={() => props.navigation.goBack()} onContinue={() => setStep(CodeStep)}/>
      }
    }

    return (
        <PreLayout>
            <HeadTitle/>
            {ShowStep()}
        </PreLayout>
    );
}

function EmailScreen({email, setEmail, onContinue, onBack}: {email: string, setEmail: (args: string) => void} & StepScreen) {
    return (
        <View style={styles.container}>
          <OpenText>
            Merci de renseigner votre adresse mail pour reçevoir un code confidentiel.
          </OpenText>
          <OpenText>
            Une fois le code reçu vous pourrez changer votre mot de passe.
          </OpenText>
          <Form>
            <FormGroup>
              <FormLabel>Adresse mail</FormLabel>
              <FormError field={"email"}/>
              <FormInput placeholder="myemail@gmail.com" onChangeText={setEmail} value={email} keyboardType={"email-address"}/>
            </FormGroup>
          </Form>
          <View>
            <ButtonPrimary text={"Continuer"} action={() => onContinue()} loading={false}/>
            <ButtonSecondary text={"Retour"} action={() => onBack()}/>
          </View>
        </View>
    )
}

function CodeScreen({code, setCode, onBack, onContinue}: StepScreen & {code: string, setCode: (args: string) => void}) {
    return (
        <View style={styles.container}>
          <OpenText>
            Le code que vous avez reçu est composé de 6 caractères.
          </OpenText>
          <OpenText>
            Si vous n'avez pas reçu votre code merci de patienter quelques minutes ou de regarder dans la boite des spams.
          </OpenText>
          <Form>
            <FormGroup>
              <FormLabel>Code confidentiel</FormLabel>
              <FormError field={"code"}/>
              <FormInput placeholder="000000" onChangeText={setCode} value={code} keyboardType={"number-pad"}/>
            </FormGroup>
            <ButtonPrimary background={GREEN} text={"Me renvoyer un code"} action={() => onContinue()} loading={false}/>
          </Form>
          <View>
            <ButtonPrimary text={"Continuer"} action={() => onContinue()} loading={false}/>
            <ButtonSecondary text={"Retour"} action={() => onBack()}/>
          </View>
        </View>
    )
}

function PasswordScreen({password, setPassword, onBack, onContinue}: StepScreen & {password: string, setPassword: (args: string) => void}) {
    return (
        <View style={styles.container}>
          <OpenText>
            Vous pouvez dès à présent modifier votre mot de passe.
          </OpenText>
          <Form>
            <FormGroup>
              <FormLabel>Mot de passe</FormLabel>
              <FormError field={"password"}/>
              <FormInput placeholder="******" onChangeText={setPassword} value={password} secureTextEntry={true}/>
            </FormGroup>
          </Form>
          <View>
            <ButtonPrimary text={"Changer le mot de passe"} action={() => onContinue()} loading={false}/>
            <ButtonSecondary text={"Annuler"} action={() => onBack()}/>
          </View>
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 16
  }
});

type ForgotStep = {
    name: string;
}

const EmailStep: ForgotStep = {name: "EmailStep"}
const CodeStep: ForgotStep = {name: "CodeStep"}
const PasswordStep: ForgotStep = {name: "PasswordStep"}

type StepScreen = {
  onContinue: () => void
  onBack: () => void
}
