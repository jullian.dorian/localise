import {StyleSheet, View} from "react-native";
import Layout from "../../components/layout/Layout";
import Form from "../../components/form/Form";
import FormGroup from "../../components/form/FormGroup";
import FormLabel from "../../components/form/FormLabel";
import FormInput from "../../components/form/FormInput";
import {useState} from "react";
import ButtonPrimary from "../../components/button/ButtonPrimary";
import {instance} from "../../auth/uri";

export default function ChangePasswordScreen(props: any) {

  const [password, setPassword] = useState<string|undefined>();

  const submitPassword = async (e: any) => {
    await instance.post("users/password", { password });
  }

    return (
        <Layout>
          <Form>
            <FormGroup>
              <FormLabel>Nouveau mot de passe</FormLabel>
              <FormInput value={password} onChangeText={setPassword} secureTextEntry={true} keyboardType={"default"} />
            </FormGroup>
          </Form>
          <ButtonPrimary text={"Changer mon mot de passe"} action={submitPassword}/>
        </Layout>
    );
}

const style = StyleSheet.create({});
