import {StyleSheet, View} from "react-native";
import Layout from "../../components/layout/Layout";
import ButtonPrimary, {RED} from "../../components/button/ButtonPrimary";
import {OpenText} from "../../components/Texts";
import ProfilePicture from "../../components/user/ProfilePicture";
import {Colors} from "../../constants/Utils";
import {useContext} from "react";
import {AuthContext} from "../../auth/auth";
import {useUser} from "../../hooks/useCachedResources";

export default function AccountScreen(props: any) {

    const [loadingUserComplete, user, setUser] = useUser();
    const {signOut} = useContext(AuthContext);

    return (
        <Layout style={{paddingBottom: 0}} areaStyle={AccountStyle.area}>
            <View style={AccountStyle.background}/>
            <View style={AccountStyle.profile}>
                <ProfilePicture />
                <OpenText style={AccountStyle.pseudo}>{loadingUserComplete && user?.pseudo}</OpenText>
            </View>

            <View style={{paddingHorizontal: 8, justifyContent: "flex-end", alignSelf: "stretch"}}>
                <ButtonPrimary text={"Changer de mot de passe"} action={() => props.navigation.navigate('ChangePassword')}/>
                <ButtonPrimary text={"Se déconnecter"} background={RED} action={() => signOut()}/>
            </View>
        </Layout>
    );
}

const AccountStyle = StyleSheet.create({
    area: {
        paddingBottom: 12,
        borderColor: Colors.darkblue,
        borderWidth: 2,
        borderTopRightRadius: 16,
        borderTopLeftRadius: 16,
        borderBottomWidth: 0
    },
    background: {
        backgroundColor: Colors.darkblue,
        position: "absolute",
        left: 64,
        right: 64,
        top: 0,
        height: 150,
        borderBottomLeftRadius: 42,
        borderBottomRightRadius: 42,
    },
    profile: {
        marginTop: 16,
        flex: 1
    },
    pseudo: {
        fontWeight: "bold",
        textAlign: "center",
        alignSelf: "center",
        marginVertical: 16,
        fontSize: 22
    }
});
