import React from 'react';
import {ActivityIndicator, StyleSheet, View} from "react-native";
import Logo, {AnimatedLogo} from "../components/logo/Logo";
import {OpenText, OpenTextTitle} from "../components/Texts";
import {Colors} from "../constants/Utils";

function LoadingScreen(props: any) {
    return (
        <View style={LoadingScreenStyle.main}>
            <OpenTextTitle style={LoadingScreenStyle.title}>Local'Ise</OpenTextTitle>
            <View style={LoadingScreenStyle.indicator}>
                <AnimatedLogo size={128} />
            </View>
            <OpenText style={LoadingScreenStyle.text}>Chargement en cours</OpenText>
        </View>
    );
}

const LoadingScreenStyle = StyleSheet.create({
    main: {
        flex: 1,
        padding: 12,
        justifyContent: "center",
        alignItems: "center"
    },
    title: {
        marginBottom: 16,
        color: Colors.white,
        textTransform: "uppercase",
        fontWeight: "bold",
        fontSize: 34
    },
    indicator: {
        flex: 0.3,
        marginTop: 24,
        alignSelf: "center",
        justifyContent: "center",
        position: "relative"
    },
    text: {
        color: Colors.white
    }
});

export default LoadingScreen;