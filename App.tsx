import React, {useState} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {Colors} from "./constants/Utils";
import useCachedResources from "./hooks/useCachedResources";
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import Navigator from "./navigator/Navigator";
import {StatusBar} from "expo-status-bar";
import LoadingScreen from "./screens/LoadingScreen";


export default function App() {

    const isLoadingComplete = useCachedResources();

    if(!isLoadingComplete) {
        return null;
    } else {
        return (
            <SafeAreaProvider style={styles.app}>
                {
                    Platform.OS === "ios" && <View style={{height: 32}} />
                }
                <Navigator />
                {
                    Platform.OS == "ios" ?
                        <StatusBar style={"dark"} translucent={true} hidden={false} networkActivityIndicatorVisible={true} backgroundColor={Colors.blue} />
                         :
                        <StatusBar style={"dark"} translucent={false} hidden={false} networkActivityIndicatorVisible={true} backgroundColor={Colors.blue}/>
                }
            </SafeAreaProvider>
        );
    }
}

const styles = StyleSheet.create({
  app:{
    backgroundColor: Colors.blue,
  }
});
