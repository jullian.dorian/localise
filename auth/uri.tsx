import axios, {AxiosInstance, AxiosRequestHeaders} from "axios";

export const baseURL = "http://192.168.52.66:3000/api/";

export const instanceAuth: AxiosInstance = axios.create({baseURL: baseURL+"auth"});

export const instance: AxiosInstance = axios.create({baseURL});

export function genHeaders(token: string | Promise<any>, headers?: AxiosRequestHeaders) {

    if(typeof token !== "string") {
        token.then((r) => {
            token = r;
        })
    }

    return {
        headers: {
            'Authorization': 'Bearer ' + token,
            ...headers
        }
    }
}
