import {createContext} from "react";

//Using memo
export const AuthContext = createContext<any>(null);

export interface AuthProps {
    token: string,
    user: UserProps
}

export interface UserProps {
    id: number,
    email: string,
    pseudo?:string,
    createdAt?:Date,
    editedAt?:Date,
    picture?: string|null,
    comments?: []
}
